\chapter{SPE Framework}
The core components of the SPE framework are:
\begin{itemize}
  \setlength{\itemindent}{1cm}
  \item SPEIntents
  \item Class Wrappers
  \item Validation Process
\end{itemize}

\section{SPEIntents}
A construct called \texttt{SPEIntent} is used to define an application’s intent with a user-private resource. SPEIntents are structured analogous to the policy model. As an example of how a \texttt{SPEIntent} would be used, consider the following: An application intends to communicate with a particular set of URLs, the communication will
be over a secure channel, and a set of credentials will be sent. In this example, the application would utilize a
\texttt{SPENetworkIntent}. In this case, a \texttt{SPEIntent} has two primary purposes: \textit{(i)} It defines what operations an application intends to perform so that at install time the user can clearly see what these operations are, and \textit{(ii)} It detects the trustworthiness of an application by identifying any attempts to violate intents by performing operations that are not defined in the intent. The potential violation of the trust can be intentional or can be caused from malware, but in either case the user is able to realize the violation.\\

\begin{figure}[h]
  \begin{center}
    \includegraphics[width = 0.7\textwidth]{dia/spesubclass.png}
  \end{center}
  \caption[Intents subclassing from main \texttt{SPEIntent} class]{SPEIntent relationships shown for intents subclassing from main SPEIntent class and common interfaces for similar intents.}
\end{figure}

All SPE intents are subclasses of the main SPEIntent class where internal properties and validation methods are defined. SPEIntent also defines a delegate interface for the consuming application to implement so that the application can be notified of results of intent validation and handle it appropriately for their application. Each subclass of \texttt{SPEIntent} contains more specific methods and properties based on the type of intent. For intents that have common methods and attributes, there is a defined interface for the intent to implement.\\

When a \texttt{SPEIntent} is validated, either a protected object is returned or an operation on a protected object is performed. Since the application may have the same intent for multiple object retrievals or operations, an intent can be re-used across multiple protected objects.
\subsection*{Chained Intents}
\texttt{SPEIntents} can have a one-to-many relationship with protected objects. However, a protected object can only have one \texttt{SPEIntent}. A single object can be related to a number of intents: a \texttt{SPEIntent}  can be chained to another \texttt{SPEIntent} depending on its type, much like how a policy can be attached to another policy. This chaining is accomplished using the Decorator design pattern\cite{14}. Chained intents provide facility in the framework so that the application can attach both a \texttt{SPENetworkIntent} and a \texttt{SPEDataPersistIntent} without bloating the implementation of the \texttt{SPELocationIntent}. It also allows re-use within the application and has a tight correlation to an attached policy where the chained intent will be validated against an attached policy if one exist, otherwise it will default to the user’s global policy.
\section{Class Wrappers}
Class wrappers are utilized to retrieve a protected object or perform an operation on a protected object. These objects can then only be instantiated and used through the SPE Framework, meaning the objects returned are immutable. To minimize changes required in the application utilizing the framework, the wrapper method’s implementation are as close as possible to the original implementation for the protected object. For example, in Objective-C if the consuming application wanted to create a \texttt{NSURLRequest} object, the changes are highlighted below:\\

\noindent \textit{Original Method:}
\begin{lstlisting}
NSURLRequest *request = [NSURLRequest requestWithURL:url]
\end{lstlisting}
\vspace{0.4cm}
\noindent\textit{Wrapper Method:}

\begin{lstlisting}
NSURLRequest *request =
        [SPEFramework requestWithURL:url
           withIntent:speNetworkIntent];
\end{lstlisting}

\begin{figure}[h]
  \begin{center}
    \includegraphics[width = 0.6\textwidth]{dia/reqprotobj.png}
  \end{center}
  \caption[Request of protected object using framework class wrapper]{Request of protected object using framework class wrapper with object being returned only after successful validation.}
\end{figure}
\paragraph*{}The class wrappers reduce the complexity of verifying the application’s use of the framework by checking to ensure that each protected object is not directly instantiated or used. On validation, the application will always be returned an immutable object—any changes to the object’s state have to be made through the framework.

\section{Validation}
The SPE Framework ensures the application is truthful by examining the \texttt{SPEIntent} and the action being performed against the user’s defined policy. If the properties within the object being requested do not match what is described in the intent (truthfulness) or if the intent violates the user’s policy (policy violation) then no object is returned and the delegate that the consuming application defined is notified. If no policy violation occurred and the intent was truthful, then the object is returned as well as a notification to the delegate that validation was successful.\\

 When an intent is validated, validation is performed within the SPEIntent object using the reflection API\cite{15} to determine the type of intent it is to validate itself appropriately. When an application needs to make a change
to the protected immutable object that it received, it again goes through the framework so validation can be per-
formed. The application invokes the corresponding operation on the framework with the immutable copy of the
protected object. This way, the framework can perform the operation in a manner that can be validated.\\

To reduce the burden of requiring the application to pass with each call the protected object’s related intent, the SPE Framework contains an \textit{Intent Cache} which maps each protected object to its related intent. When validation is performed, the process can be complex for properties that hold variable data. For example, consider an \texttt{HTTP POST} request where the body of the request can hold a variable amount of privacy data. Validation in this scenario is more complex and will consume more resources as they have to be checked for any security or privacy sensitive data. To perform this validation, SPE delegates the responsibility of checking this variable data to the \textit{Data Inspector}. The \textit{Data Inspector}’s responsibility is to examine chained intents on the protected object for any privacy or security data being utilized and then validate both the object and the intent to ensure that the intent is being truthful. To improve the efficiency of this process, the Data Inspector examines the \textit{Intent Cache} for sensitive data that has been released to the application and check for that data to exist. This reduces resource consumption by not having to check each privacy and security sensitive component on the mobile device and only check those that have been accessed through the SPE Framework.
\begin{figure}[t]
  \begin{center}
    \includegraphics[width = 0.75\textwidth]{dia/reqmut.png}
  \end{center}
  \caption[Request of mutation of previously acquired protected object]{Request of mutation of previously acquired protected object, mutation is created on copy, validated with cached intent, and returned as immutable.}
\end{figure}

### Instructions

Just compile *report.tex*. All other files are included in it.

**Bibliography** added using BibTex. Refer [BibTeX on wikibooks](https://en.wikibooks.org/wiki/LaTeX/Bibliography_Management#BibTeX). File is *ref.bib*.
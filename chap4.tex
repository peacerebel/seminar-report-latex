\chapter{Implementation}
Some salient details of the implementation of SPE for iOS is discussed in this chapter. Most related research is targeted at Android, since that source code base is open to modification. But, the goal of this framework is to be immediately available without modifying the OS.

\section{SPEIntents}
The \texttt{SPEIntent} class is used for validating itself against defined policies whether they be global policies or attached policies. Several protocols are created to group like SPEIntents. Some of these are:  \texttt{SPEChainableIntentProtocol}, which allows for chained intents, and \texttt{SPESensorDataIntentProtocol}, which allows the application to set desired and minimum data accuracy requirements.\\

The class hierarchy of SPEIntent and its subclasses are analogous to the policies discussed in previous chapters. For example, with a SPEPhotoIntent an application can specify that it intends to modify photos, that it will require the date attribute, and that it will require the location attribute. The corresponding policy allows the user to set restricted dates, restricted locations, and if the application can read the date and location attributes as well as write to the library. Within a policy a user can permit the application to read photos. However within a SPEIntent, read access is not specified as it is implied a read operation would occur.

\section{Validation}
All validation is performed within the \texttt{SPEIntent}. This allows for maximum re-use of validation within the intent. When performing the validation there are two areas that SPE focuses on: (1) Is the \texttt{SPEIntent} \textit{truthful} with its action, and (2) Does the \texttt{SPEIntent} \texttt{comply} with the user defined policy. To check if the \texttt{SPEIntent} is truthful, we improve the efficiency of how this is performed by tainting properties and operations of a protected object using a class wrapper. To validate a policy intent, we use an internal representation of the policy database in memory for the application and check the operations and instantiations of the protected object.\\

To validate attached data policies and chained \texttt{SPEIntent}s, a \texttt{SPEShareableDataPol\\icy} protocol that allows a \texttt{SPEPolicyDataPersist} and/or \texttt{SPEPolicyNetworkBran\\ch} to be attached to a privacy \texttt{SPEIntent} are created. For chained intents, a \texttt{SPEChainabl\\eIntentProtocol}, which allows intents to be chained to a privacy related intent, is created.\\

\noindent The general validation algorithm is as follows:
\begin{lstlisting}[frame = lines]
validation = true
if Validate Privacy Intent to Privacy Policy then
  for all Chained Intents as Chained Intent do
    if not Validate Chained Intent to Attached Policy then
      validation = false
      break
    end if
  end for
else
  validation = false
end if
\end{lstlisting}

To cache protected objects, a \texttt{NSDictionary} is used to store a mapping to a singleton copy of the validated object during the life of the application. If a validated object was a singleton itself, an identifier in the cache to the singleton is stored.
\subsection{Sensor Accuracy and Privacy}
The goal is to provide broad methods of preserving privacy and show its effectiveness and effect on the OS. With the location policy, there are several levels that a user can define in their policy: precise, anonymous, generalized, no data, or bogus data. If the policy setting is \textit{anonymous}, a random number generator is used to generate a random number between -.04 and .04 and add it to the latitude and longitude (approximately a 2 mile radius around a given location). To compute the \textit{generalized} location, take only one significant digit for latitude and two significant digits for longitude which provides a consistent generalized location for a user given a specified area they exist in. If the policy setting is bogus, generate a random location by generating random latitude and longitude values, and if the policy setting is no data we simply return nothing.\\

For implementing a class wrapper for location in iOS, a delegate is used when retrieving locations, so it is necessary to proxy the data being retrieved from the device. To accomplish this, a proxy delegate that receives location updates, heading updates, and region updates is implemented. It will then notify the delegate within the application if validation passes of each location update. The proxy delegate also performs fuzzing and scrubbing based on the accuracy set. So if a user set their permitted location accuracy to \textit{generalized}  , the proxy delegate will capture the data and fuzz it before it is returned to the application. By utilizing a proxy delegate, we minimize the changes an application would need to make to gather locations from a user.

\begin{figure}[t]
  \begin{center}
    \includegraphics[width = 0.7\textwidth]{dia/proxy_delegate.png}
  \end{center}
  \caption[How proxy delegate utilized in SPE Framework]{How proxy delegate utilized in SPE Framework to enforce location scrubbing and policies.}
\end{figure}

\subsection{Proxying Privacy Data}
Several libraries require that privacy data is enumerated using a block in Objective-C. This adds complexity for SPE to perform validation on the data being returned as the block can be called asynchronously. However much like a proxy delegate, a proxy block is implemented which will check the data being sent back from the library being consumed and perform validation and scrubbing as necessary. An example of this is when accessing the photo library, ALAssets are eturned through a block. SPE will utilize the proxy block to wrap the original block passed in from the application to proxy the request and perform the necessary validation. This again allows to check against the policy and intent for both truthfulness and policy violations. Additionally, the proxy block allows the framework to scrub data such as removing date and location attributes from a photo if a user’s policy did not permit access to those elements.

\subsection{Temporal and Spatial Restrictions}
When a user defines a location restriction, he/she can specify a point and the radius around that point to specify a restricted region. This type of spatial restriction is represented as a \texttt{CLCircularRegion} in iOS. To test if a location falls within one of these restricted regions, utilize the iOS convenience method in the \texttt{CLCircularRegion} class:

\begin{lstlisting}
-(BOOL)containsCoordinate:
(CLLocationCoordinate2D)coordinate;
\end{lstlisting}

Temporal restrictions can be applied to location as well as other policies including photos. The combination of these temporal and spatial policies allow a user to define with more granular access to their privacy data.
\subsection{Notifications}
To ensure that the application implementing the framework can handle different policy settings, the application can implement the \texttt{SPEResponseDelegate} protocol, which includes two methods: \texttt{didReceiveSuccessfulValidation} will be called with the successful intent, and \texttt{didReceiveDeniedValidation} will be called with an \texttt{NSError} object and the SPEIntent that was denied. In order to ensure that the user is informed of any kind of policy violations and truthfulness mismatches with the intent, the framework sends a \texttt{UILocalNotitification} that also shows a \texttt{UIAlertView} and allows the user to dismiss future alerts. If the user dismisses future alerts, they can still see the notifications in the \emph{Notification Center}.
\section{SPE Conversion Assistant and Policy Management}
The \emph{SPE Conversion Assistant} is a point-and-click solution that will inject the SPE Framework into an application’s source code. It does this by injecting code into the application delegate that swaps out implementations of protected methods with SPE’s own implementation. Once the call is intercepted, SPE performs the validation just as if the call was made to it directly through one of the class wrappers. The SPE Framework is compiled into a static library that is linked to the application when it is built.\\

Using the \emph{SPE Policy} application a user can generate a policy and store it on a dedicated web server or a cloud service of their choosing. Because the policy file can be publicly accessible, it is encrypted using a credential-based key using the PBKDF2 algorithm. When an SPE-converted application is first launched, SPE will prompt the user for the location of the policy file and the credentials to decrypt it. It will then download the file and install it for utilization by the SPE Framework. If the application is restarted, SPE will check if a policy file was installed and prompt the user to unlock the existing policy file using their credentials or it will allow the user to download a new policy file.

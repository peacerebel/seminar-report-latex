\chapter{Introduction}
Mobile technology has accelerated the pace at which people access, acquire and generate data. Users’ cell phones are now rich repositories of memories and content that chronicle their lives. A staggering archive of personally identifiable information exists about cell users - a reality that is both the consequence of and driving force of the networked age. As nearly one-fifth of cell owners (17\%) use their cell phone for most of their online browsing, privacy and data management on mobile devices is increasingly emerging as a contested arena for policymakers, industry leaders and the public\cite{1}.\\

Mobile phones and tablets are, fundamentally, just small computers that we carry around in our bags and pockets. But although we carry these computers more often and closer than traditional laptops, and although they are equipped with sensors to record many aspects of our lives, their hardware and operating systems have been designed to give us very little insight and control over them. While they provide us with seemingly unlimited amounts of useful tools, most of us don’t consider the massive amount of personal data that we carry around in our smartphones.
According to the Pew Internet's report, smartphone owners are especially vigilant when it comes to mobile data management. Six in ten smartphone owners say  they back up the contents of their phone; half have cleared their phone’s search or browsing history; and one third say they have turned off their phone’s location tracking feature\cite{1}.\\

A private firm, Lookout Security, found far more invasive practices by the companies that serve up advertisements on behalf of app  developers. Some pushed advertisements to the device notification bar, well beyond the confines of the actual application. Others inserted new icons or shortcuts on the mobile desktop and tweaked bookmarks settings\cite{2}.\\

Service providers (like AT\&T, Sprint, Verizon, and T-Mobile) collect data, but are not forthcoming in detailing exactly what data they collect, the reasons they collect it, and their data retention policies. At the very least, smartphone service providers collect the following:
\begin{itemize}
  \item Incoming and outgoing calls: the phone numbers you call, the numbers that you receive calls from, and the duration of the call,
  \item Incoming and outgoing text messages: the phone numbers you send texts to and receive texts from,
  \item How often one checks e-mail or access the Internet,
  \item Location.
\end{itemize}

\begin{figure}[h]
  \begin{center}
    \includegraphics[width = 0.9\textwidth]{dia/graph5.png}
  \end{center}
  \caption{Comparing owners on mobile data management and security}{Smartphone owners vs. other cell owners on mobile data management and security\cite{1}.}
\end{figure}

Data retention policies vary among service providers, and certain records are kept longer than others.  For instance, as of September 2011, Verizon, T-Mobile, AT\&T and Sprint all differ when it comes to how long they store any combination of cell tower history records, text message detail, text message content, IP session information, IP destination information, and bill copies.\\

In addition to the data collected by smartphone service providers, we should also be aware of the possible privacy issues surrounding the collection or disclosures of:
\begin{itemize}
  \item Any photos or video take on the phone,
  \item Details about the text messages and e-mails we send and receive, including the content,
  \item Who is calling us, who we are calling, and details about the phone call such as when it was placed and how long it lasted,
  \item The contacts stored in the phone,
  \item Passwords,
  \item Financial data,
  \item Phone's calendar data,
  \item Location, age, and gender.
\end{itemize}

Today, many privacy breaches are report in smartphone apps. Apps are widely-used by advertisers to capture your smartphone data. Advertisers want to market to the people who are most likely to buy their product or service. The more information they collect, the better their ability to know the types of products and services the people are most likely to buy. Therefore, they are very interested in what the smartphone has to “say” about each person as a consumer. The advertisers supply code to the app-makers to build into the app. The code not only makes an ad appear when we use the app, but also collects data from the phone and transmits it back to the advertiser. It’s also possible that the app itself collects data which is shared with ad networks.  The ad networks may then show the user ads that contain content based on the data collected\cite{3}.\\

Law enforcement has also been known to tap into the locations of smartphones, ask wireless providers to turn over days’ worth of location data, and implant tracking devices. Also, law enforcement can request all the data the smartphone provider has collected about an individual. Federal privacy laws have not kept up with the pace of technology and courts are unclear on how easy it should be for law enforcement to gain access to one's smartphone and its data\cite{4}.\\

The popularity and increasing availability and quantity of downloadable apps is a top privacy issue. People are increasingly spending more time using mobile applications than they are browsing the mobile web. There are hundreds of thousands of apps available for your smartphone, and anyone can create an app. The app marketplace is filled with numerous free or low-priced choices.  Apps can collect all sorts of data and transmit it to the app-maker and/or third-party advertisers. It can then be shared or sold.  Apps may also be infected with malware. Even an app as seemingly harmless as a flashlight, game or radio might collect such information as your device ID, your contacts and/or your location\cite{5}.\\

The rapid growth in the mobile device ecosystem demands viable solutions security and privacy concerns. Even though mobile devices are becoming more powerful, there still exist constraints on computing power, memory capacity, and a virtual endless supply of energy that traditional computing platforms offer today. These constraints limit mobile devices from performing computationally expensive operations such as pattern-based intrusion detection or fuzzy checking of privacy leakage. Even if computing power on mobile devices were to increase, the effect on the device’s battery would be unacceptable for a user. Additionally, the user experience may be affected if more computationally expensive operations are being executed while the user is interacting with the device.\\

There has been good amount of work on enhancing privacy and security on mobile devices. \emph{Taming Information Stealing Smartphone Applications (TISSA)}\cite{6} is built on top of existing Android security mechanisms. It provides a lightweight protection and apllication transperancy. \emph{TISSA} focused on four types of user datas: contacts, phone identity, call logs, and location data. It provides the option to return the three values - no data, anonymous, and bogus for fake values for the request of the four user data types specified.\\

\emph{Identidroid}\cite{7} focuses on anonymity by proposing a custom Android OS that makes sure application do not identify user. The \emph{Android Runtime Security Policy Enhancement Framework (SEAF)}\cite{8} focuses on dynamic behaviour of the application and validates applications by exercising permission patterns. This exhibits low overhead and require modification in Android OS.\\

\emph{TaintDroid} is another method which uses dynamic taint analysis to monitor variables, method calls, and data on the filesystem to determine if data is being leaked\cite{9}. \emph{AndroidLeaks}\cite{10} propose a static analysis framework for Android using WALA and performing reachability analysis.

\section{Why SPE Framework?}
The SPE Framework provides additional protections without requiring a modification to the OS or the device to be jailbroken, a problem that has not been solved in other research. It does require modification to the application and is machine-automated, where the developer pass the source code of the application through the injection framework.
\subsection*{Advantages over other methods}
\begin{itemize}
  \item When an OS is updated, a consumer does not have to wait for other solutions to be incorporated into the OS since SPE is integrated within the application. The framework would only need to be updated when significant API changes are made.
  \item Users would not need to jailbreak their device in order to adopt SPE. 
  \item SPE can be utilized on a stock device running a stock OS. This approach allows maximum penetration into the user base as users do not need a specific OS or device.
  \item Since SPE intercepts calls to security and privacy sensitive resources, it can guarantee that a consumer’s policy is enforced. Unlike other solutions, the framework does not attempt to recognize patterns where false positives and false negatives can be returned.
  \item A consumer will not lose the inherent trust with an OS or void their warranty by utilizing a modified OS or jailbroken device.
\end{itemize}
\subsection*{Contributions of the SPE framework\scriptsize\cite{12}}
SPE provides a verbose fine-grained policy model to allow users to define additional security and privacy controls on a mobile device. These policies include constraints on precision as well as temporal and spatial properties of sensing data, and more.\\

It implements a novel intent-based validation engine; each application must specify its intent in order to obtain access to a protected resource. If an application wishes to share any data it is permitted to access, it must specify an additional “chained intent” stating such a purpose. This allows the framework to understand how the application intends to access data and what it intends to do with the data.\\

SPE allows the user to understand an application’s data needs, and to check if an application is truthful. To enforce this, SPE will verify that the application does not perform an action that is not explicitly permitted. SPE also helps in detecting application poisoning where a trusted application’s execution is modified by configuration data.

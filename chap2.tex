\chapter{Policy Model}
The policy model used by the SPE Framework is derived from an ontology for enforcing security and privacy policies on mobile devices\cite{11}.The policies defined in the ontology are divided into two main categories: \emph{Security} and \emph{Privacy}. Another type of policy defined in the ontology is the \emph{ChainablePolicy} which links both a security policy and a privacy policy to provide more fine grained policies.

\section{Security}
The main goal in this category is to define policies around data transferred or persisted, authentication data, and ensuring that they are handled using the proper security mechanisms such as transport layer security or encryption. The policies here can be used for both jailbroken and non jailbroken devices. There are three categories within the security policies - \emph{Data Persistence}, \emph{Communication}, and \emph{Credentials}.
\subsection{Data Persistence}
The primary objective is to ensure that any data persisted on the device would be properly secured using the minimum encryption level. Additionally with the availability of syncing data to cloud based storage providers, it adds the ability for a user to specify policies on permitting data to be synced. This policy could be used to ensure that sensitive data is encrypted on mobile devices in case they are lost or stolen, and that sensible data would not leave the local network.\\

These policies are specified with \textit{Can Persist to Device, Data Encryption Level}, and Can \textit{Persist to Cloud}. The goal with this policy is to allow consumers the ability to specify how data should be encrypted. The \textit{Data Encryption Level} is dependent on the platform. By enforcing encryption, a user can ensure that if their device is ever lost or stolen that the data is secure.

\subsection{Communication}
This policy contains properties that define what domains the application can communicate to and any transport layer security requirements on the transmission of data. This can be be used to restrict any privacy leakage of an application that sends data on behalf of the application to third party servers (for example, for the purpose of tracking user online behaviors). Additionally for any sensitive information that is in transit, a user can ensure that they are using transport layer security.\\

A user can define a \textit{Domain Policy} that has the following attributes: \textit{Permit Data, Require SSL, Permitted Credentials,} and \textit{Credentials Require SSL}. These attributes specify if a domain can receive data, if the connection requires SSL, the credentials that can be transmitted, and if SSL is required to send credentials. Each domain policy is specified for a particular domain but can also contain wildcards to cover multiple domains, for eg.: \textit{*.google.com}.

\subsection{Credentials}
This policy ensures that specific policies around a user credential being transmitted over a network could contain its own restrictions. It specifies a basic permission: \textit{Access Allowed}. This permission allows the user to specify a global policy on whether a particular credential can be used by another application. This credential could be a username/password combination to their email, banking account or any other service that would require authentication.

\begin{figure}[h]
  \begin{center}
  \includegraphics[width=0.9\textwidth]{dia/policy_model_policy_elements.png}
\end{center}
\caption{Relationship of policy elements within policy model}
\end{figure}

\section{Privacy}
Both iOS and Android provide general privacy controls that allow an application access to user’s photos, contacts, location, and other personal data. These controls provide an all or nothing access to a specific class of a user’s personal data. Currently users  cannot define finer granular controls such as allowing an application to only gather location data in specific locations, restricting the access to certain photos from an application, or not allowing access to a subset of contact records.
\subsection{Sensors}
With each sensor, a \textit{Data Accuracy Allowed} parameter to be defined. This allows the user to specify precise data, anonymous data, generalized data, no data, or bogus data from a sensor. For example with location, a \textit{generalized} sensor accuracy policy would return a consistent latitude/longitude for a particular region where \textit{anonymous} could be any point in particular region.\\

Users can also define spatial and temporal permissions for sensors, such as: \textit{Time Restrictions} and \textit{Location Restrictions}. To remove the risk of an application detecting a user’s exact location by specifying small regions and detecting when a user enters those regions, we add an additional permission of \textit{Permit Regional Monitoring With Restrictions}. This policy also ensures if regional monitoring occurs within a designed spatial or temporal restriction, that regional reporting will not occur. These policy settings allow a user to have more fine grained control on how their location data is accessed.
\subsection{Multimedia}
A user can specify a policy that separately defines read and write permissions. Multimedia can also contain metadata that describes where and when the element was captured. A user can specify additional policies to protect these attributes by specifying \textit{Permit Read Photo Date Attribute} and \textit{Permit Read Photo Location Attribute}.\\

Spatial and temporal restrictions can be placed on multimedia as well. For example, a user could define that an application does not have access to photos that were taken during certain dates or at certain locations. Additionally, a user can specify restricted regions where if a photo was captured in the region, an application cannot access the photo. A user can also specify specific elements within multimedia that an application cannot access, so if a user wants to restrict specific photos, they are able to. For all of the policies defined, a user can selectively specify which calendar events, photos and other multimedia, and contacts that cannot be accessed by the application. This allows a user to ensure that a sensitive piece of privacy information is not accessible by a mobile application.
\subsection{Contacts \& Calender}
Contacts and calendar data is typically granted access to the entire library with read and write permissions. This is a concern as this data can be leaked: the social networking application Path was found to upload a user’s entire address book without the user knowing\cite{13}. Additionally, contacts and calendar data contain specific attributes that an application may not require access to such as first name, last name, address, phone numbers, and email addresses. To protect against misuse of this data, a user can specify \textit{Permit Read and Permit Write, Restrict Address Access, Restrict Phone Number Access,} and \textit{Restrict Email Address Access}.\\

An example of how these enhanced policy attributes could be used, if a user was using an application to make phone calls, the user could set \textit{Permit Write = false, Restrict Address Access = true, Restrict Email Address = true}.As with other privacy data, calendar data allows for general read/write access. Additionally, with calendar data, a user can specify \textit{Restricted Dates}. For both contacts and calendar, a user can restrict access to specific contacts or events.
\section{Chainable Policy}
The primary goal of a Chainable policy is to allow a user to attach several security policies to a privacy policy. This allows a user to specify that an application can gather its location but gathered location data cannot be transmitted over a network. The policy also enables a user to allow an application to access a user's photos but not write them to the local disk unencrypted. A Chainable policy gives the user a deeper level of control of their privacy data by allowing security and privacy policies to be combined together. This is implemented within the SPE Framework to provide more fine grained control of when an application gains access to privacy elements, how and where that data can be transmitted or persisted to disk.
\section{Defining a Policy}
Consider the situation where a user wants to define a policy as, “Facebook can have my general location with exception between 7am and 5pm, but not my home or work location and can only share this data to domain facebook.com over SSL”.\\

This policy is defined as follows:
\begin{itemize}
  \setlength{\itemindent}{1cm}
  \item Define Location Policy 
    \begin{list}{-}{}
      \item Set \textit{Data Accuracy Allowed} = Generalized
      \item Add \textit{Time Restriction} = {700, 1,700}
      \item Add \textit{Location Restriction} = Home
      \item Add \textit{Location Restriction} = Work
    \end{list}
\end{itemize}
\begin{itemize}
  \setlength{\itemindent}{1cm}
  \item Define Domain Policy
  \begin{list}{-}{}
    \item Set \textit{Permit Data} = TRUE
    \item Set \textit{Require SSL} = TRUE
    \item Add \textit{Permitted Domain} = ”*facebook.com”
  \end{list} 
\end{itemize}
\begin{itemize}
\setlength{\itemindent}{1cm}
  \item \textit{Location Policy} attach \textit{Domain Policy}
\end{itemize}
A user would create this policy through the \emph{SPE Policy} application. \emph{SPE Policy} allows the user to define a policy and make it accessible to applications.

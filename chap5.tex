\chapter{Evaluation of the SPE Framework}
The primary goal of the SPE Framework is to protect the user from apps that misuse personal data. Such misuse may either stem from malicious design on the part of the developer, or it may be “accidental”: a developer may use a third-party library, which in turn misuses personal data. The SPE Framework provides a way for developers to be open and clear about their intentions of how personal data from a user is being used. The SPE framework requires the source code of the application that is being monitored. The SPE Conversion Assistant is utilized as it allows an application to be easily integrated with the SPE Framework.

\section{Security and Privacy Enhancements Evaluation}
Evaluation is performed on a set of open-source iOS apps that are available for download from the iTunes App Store. Out of the 14 apps that were tested, only two did not report a violation.
\begin{table}[h]
  \centering
  \fontsize{9pt}{11pt}\selectfont
  \caption{List of Apps Used for SPE Framework Evaluation}
  \begin{tabular}{p{0.15\textwidth} p{0.35\textwidth} p{0.4\textwidth}}
  \hline
  \textbf{App Name} & \textbf{Description} & \textbf{Nature of Violation} \\
  \hline
  AlienBlue & Commercial open source Reddit client for iOS & No violations reported\\
  \hline
  CamLingual & Read and tranlsate signs using OCR & Location/Unencrypted Write to Disk\\
  \hline
  Congress & Application for tracking activity in congress & Block Google Analytics Data /Locaion\\
  \hline
  CycleStreets & Plan a cycle journey within the UK & Location/Unencrpted Write to Disk/Network\\
  \hline
  Doppio & Allow you to find closest Starbucks location & Location\\
  \hline
  FoxBrowser & Full fledged browser with Firefox Sync support & Block Google Analytics Data /Location/Unencrypted Write to Disk\\
  \hline
  Hacker News Client & Social news website on computer hacking and startup companies & No violations reported\\
  \hline
  IRCCloud & IRC Chat Client & Insecure Network\\
  \hline
  Plain Note & Simple notetaking client with ability to synchronize notes & Insecure Sync of Notes on Network\\
  \hline
  Scanvine & News Aggregator & Unencrypted Write to Disk\\
  \hline
  Sol & Weather Application & Location\\
  \hline
  Spika & Instant messaging apllication & Unencrypted Write to Disk /Network/Location\\
  \hline
  The White House & shell Application that White House utilizes & Block Google Analytics Data\\
  \hline
  Wikipedia & Popular crwodsourced internet encyclopedia & Location\\
  \hline
  \end{tabular}
\end{table}

For this evaluation a policy is built using the \emph{SPE Policy} application that would only allow network communication to the required domains for the targeted application. The policy did not allow access to privacy information on the device, which allowed testers to monitor what privacy elements were accessed by the application as the framework would alert violations to the policy. The application has access to the policy file. Once this file is generated, it was then stored on Google Drive and made publicly accessible via a dedicated URL. The SPE Conversion Assistant is used to inject the SPE Framework into each application.\\

When opening the application the SPE Framework prompts for both a URL to the policy file and the credentials to decrypt the policy file. After the policy was downloaded to the device and installed, we then ran the application and performed any required account setup that may be required to utilize the application. Using the policy that is defined, it was able to determine what violations the SPE Framework were able to find, and adjust the policy as needed to allow the application to perform its expected operations.
\subsection*{Location Violations}
Among those tested, the applications that had a legitimate use case for the user’s location were CycleStreets, Doppio, Sol, and Wikipedia. With CycleStreets the location was being requested before the user set up a route based on their current location. With Doppio and Wikipedia, a user may want to provide an anonymous location within their region so that they can view information around their current location without providing their exact location. Here, SPE can be utilized to enforce generalized or anonymous location data.\\

Camlingual and Spika did not have legitimate use cases. Camlingual’s sole functionality is to provide OCR capabilities to photos that a user takes using their mobile device; access to the user’s location is not required. Spika requested the location of the user even though it was not required for the application to function. FoxBrowser was an exception to applications requesting access to the user’s location. FoxBrowser did not explicitly request the user’s location, however if the user visited a webpage that requested the user’s location, the request would be made. Because the SPE Framework utilizes method swizzling to intercept calls to protected resources, even though the location request was not explicitly defined in FoxBrowser, SPE was still able to enforce a location policy.
\subsection*{Network Violations}
In several apps the SPE Framework was able to block data leaving the device to unauthorized endpoints, over untrusted channels, or to the Google Analytics service. In CycleStreets, the SPE Framework was able to block network requests that were not destined to the openstreetmap.org domain. In Plain Note, the framework was able to block notes from being synchronized over a non-SSL channel.In Congress, FoxBrowser, and the White House, the framework was able to block data being transmitted to the Google Analytics service.
\subsection*{Disk Violations}
There were several apps (CamLingual, CycleStreets, Scanvine, and Spika) that attempted to write data to disk without explicitly requiring the data to be encrypted. This is a concern as this data could contain user information and if the device is ever lost or stolen, a malicious user could gain access to this data without unlocking the device.
\subsection*{}
The evaluation presented here can be performed against any application where the source code is available. Instead the evaluation included any application for which the source code can be obtained, build, and run the application in the simulator or device. With all apps that fit this criteria, the SPE Framework is able to be successfully integrate within the application using the \emph{SPE Conversion Assistant}. Because the SPE Framework intercepts requests to methods that request access to security and privacy sensitive elements, the SPE Framework is able to guarantee enforcement of the user’s defined policy.

\section{Performance Evaluation}
To evaluate the overhead an application that focused on individual units of the framework is created. All test cases were repeated one hundred times on an iPhone 5 and the results were averaged. The performance tests were focused on individual components of the framework to clearly identify where more overhead was observed.
\subsection*{Network Access}
\begin{figure}[h]
  \begin{center}
    \includegraphics[width = 0.6\textwidth]{dia/graph1.png}
  \end{center}
  \caption[Network testing with NSURLRequest]{Network testing with NSURLRequest—intent truthfulness, abiding policy, credential injection}
\end{figure}

To evaluate network access a policy that allowed communication to a specific domain, required SSL, and permitted a credential to be sent over SSL is utilized. In Test A the application’s intent was truthful and it followed the userdefined policy. In Test B the application attempted to send a network request to a domain it did not intend to, simulating an intent being untruthful. In Test C, the intent was truthful, however it did not abide to the policy. Finally in Test D, the intent was truthful however it attempted to inject credentials in the request it was not permitted to use from the user policy.
\subsection*{Data Persistence}
To evaluate data persistence, a medium sized image (378 KB) is written to the local filesystem. In Test A, the intent was truthful and the policy was followed. In Test B, the intent was untruthful but the policy was followed. In Test C, and D, we mirrored Test A and B respectively with requiring encryption.
\begin{figure}[h]
  \begin{center}
    \includegraphics[width = 0.6\textwidth]{dia/graph2.png}
  \end{center}
  \caption[Data persistence testing with NSData]{Data persistence testing with NSData—intent truthfulness and encryption}
\end{figure}

In contrast to the network evaluation, the difference here is smaller between operations that use SPE and those that do not. This can be attributed to the actual data persist operation being performed whereas in the other case, the request is never sent over the network. We also observed that when not using encryption, the validation and execution of tasks using the SPE Framework contains less of an overhead.
\subsection*{Photo Library}
\begin{figure}[h]
  \begin{center}
    \includegraphics[width = 0.6\textwidth]{dia/graph3.png}
  \end{center}
  \caption{Photo library access with scrubbing and intent truthfulness}
\end{figure}

To evaluate photos 25 photos on the device are enumerated, retrieved date and location attributes, and created a UIImage object from the photo. In Test A, SPE was utilized to scrub date and location attributes of the photo with a truthful intent. Test B was similar to Test A, however with the intent not being truthful. Test C did not scrub date and location attributes and the intent was truthful. Finally in Test D, SPE was not used at all.\\

Utilizing SPE showed a marginal overhead even when attributes were scrubbed. This is in stark contrast to the network evaluation, but unlike the network evaluation policies were enforced against properties of a photo and not examining data for potential privacy leakage. With photo evaluation, only one non-SPE test is included as there is only primarily one method to access the photo library.
\subsection*{Location Evaluation}
Evaluation of location sensing is interesting as there is an inherent delay in receiving location updates. Twenty-five location updates were received five times to get an average response time. Test A evaluated a spatial restriction that the device did not violate and likewise Test B utilized a temporal restriction that the device did not violate. Test C used anonymization to “scrub” the location data. In Test D, the SPE framework was not used to enforce policies or use the proxy delegate described earlier.
\begin{figure}[h]
  \begin{center}
    \includegraphics[width = 0.7\textwidth]{dia/graph4.png}
  \end{center}
  \caption[Location testing]{Location testing with anonymization, spatial and temporal restrictions.}
\end{figure}
The computing overhead appeared small compared to the overall operation of receiving location data.
\subsection*{}
The performance evaluation examined several different permutations of using the SPE Framework. Only in the network evaluation a significant proportional overhead is observed. This overhead was expected as the data within a network request is inspected for any privacy data that was requested and may have been leaked. he policy that the user defines will also have an impact on performance. For example, the more restricted locations a user specifies, the longer it would take for the framework to enforce the policy.\\

In this evaluation, primary focus was on execution time to measure the performance overhead of SPE and to ensure that there was no delay in user experience. CPU and energy is more difficult to measure in the targeted evaluation we created that evaluated different aspects of utilizing SPE. However both CPU and energy can be expressed as functions of execution time, at least to a limited extent where they would be proportional.
